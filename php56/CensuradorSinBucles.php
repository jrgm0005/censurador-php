<?php

namespace Censurador;

include_once("CensuradorAbstracto.php");

class CensuradorSinBucles extends CensuradorAbstracto
{
    private $array_reemplazo;

    public function __invoke($texto) : string
    {
        $this->check_texto_y_palabras_a_censurar($this->palabras_censuradas, $texto);
        return $this->censurar_no_bucle($this->palabras_censuradas, $texto);
    }

    private function convertir_a_asteriscos($palabra) : string
    {
        return str_repeat("*", strlen($palabra));
    }

    private function censurar_no_bucle($palabras, $texto) : string
    {
        if(is_array($palabras))
        {
            array_walk($palabras, array($this, 'crear_array_reemplazo'));
            $frase_final = str_ireplace($palabras, $this->array_reemplazo, $texto);

            return $frase_final;
        }
        else
        {
            $reemplazo = str_repeat("*", strlen($palabras));
            $frase_final = str_ireplace($palabras, $reemplazo, $texto);

            return $frase_final;
        }
    }

    private function crear_array_reemplazo($palabra)
    {
        if(empty($this->array_reemplazo))
            $this->array_reemplazo = array();

        array_push($this->array_reemplazo, $this->convertir_a_asteriscos($palabra));
    }
}

?>
