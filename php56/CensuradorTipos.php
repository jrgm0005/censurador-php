<?php

namespace Censurador;

include_once("CensuradorAbstracto.php");

class CensuradorTipos extends CensuradorAbstracto
{
    public function __invoke($texto) : string
    {
        $this->check_texto_y_palabras_a_censurar($this->palabras_censuradas, $texto);
        $texto_class = new Texto($texto, $this->palabras_censuradas);

        return $texto_class->censurar();
    }
}


// Clases auxiliares

class Texto
{
    public $texto;
    public $palabras_censuradas;

    public function __construct($texto, $palabras_censuradas)
    {
        $this->texto = $texto;
        $this->palabras_censuradas = array();
        foreach ($palabras_censuradas as $key => $palabra)
        {
            array_push($this->palabras_censuradas, new Palabra($palabra));
        }
    }

    public function censurar() : string
    {
        foreach ($this->palabras_censuradas as $key => $palabra_censurada)
        {
            $this->texto = str_ireplace($palabra_censurada->palabra, $palabra_censurada->dame_palabra_censurada(), $this->texto);
        }

        return $this->texto;
    }
}

class Palabra
{
    public $palabra;

    public function __construct($palabra)
    {
        $this->palabra = $palabra;
    }

    public function dame_palabra_censurada()
    {
        if(empty($this->palabra))
            return "";

        return str_repeat("*", strlen($this->palabra));
    }
}

?>
