<?php

namespace Censurador;

include_once("CensuradorAbstracto.php");

class CensuradorSimple extends CensuradorAbstracto
{
    public function __invoke($texto) : string
    {
        $this->check_texto_y_palabras_a_censurar($this->palabras_censuradas, $texto);
        return $this->censurar_palabras($this->palabras_censuradas, $texto);
    }

    private function censurar_palabras($palabras, $texto) : string
    {
        foreach ($palabras as $key => $palabra)
        {
            $texto = $this->censurar_palabra($palabra, $texto);
        }
        return $texto;
    }

    /**
     * [censurar función que se le pasa una palabra a censurar y un texto. Devolverá el texto con la palabra censurada sustituida por * ]
     * @param  [type] $palabra [string]
     * @param  [type] $texto   [string]
     * @return [type]          [string]
     * Texto después de censurar la palabra definida
     */
    private function censurar_palabra($palabra, $texto) : string
    {
        $reemplazo = $this->convertir_a_asteriscos($palabra);
        $frase_final = str_ireplace($palabra, $reemplazo, $texto);
        return $frase_final;
    }

    private function convertir_a_asteriscos($palabra) : string
    {
        return str_repeat("*", strlen($palabra));
    }
}

?>
