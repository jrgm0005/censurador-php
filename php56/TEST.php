<?php

namespace Censurador;

// Incluímos los 3 censuradores construidos
include_once("CensuradorSimple.php");
include_once("CensuradorTipos.php");
include_once("CensuradorSinBucles.php");


// Se incluye estas sentencias, para depurar errores.
// error_reporting(E_ALL);
// ini_set('display_errors', '1');

// Arrays de palabras y el texto a censurar
$palabras = array('1' =>  "suerte", '2' => "talento", '3' => "vida" );
$texto = 'Aquel que dijo: "mas vale tener suerte que talento",
conocia la esencia de la vida. La gente tiene miedo a
reconocer que gran parte de la vida depende de la suerte,
asusta pensar cuantas cosas escapan a nuestro control.';

$palabras2 = array('1' =>  "conseguir", '2' => "averiguar");
$texto2 = 'Conseguir lo que quieres es tan dificil
como no conseguir lo que quieres. Porque entonces tienes
que averiguar que hacer con ello,
en lugar de averiguar que hacer sin ello.';

// Prueba de los censuradores
prueba_censurar_simple($palabras, $texto);
prueba_censurar_simple($palabras2, $texto2);
prueba_censurar_tipos($palabras, $texto);
prueba_censurar_tipos($palabras2, $texto2);
prueba_censurar_sin_bucles($palabras, $texto);
prueba_censurar_sin_bucles($palabras2, $texto2);

function prueba_censurar_simple($palabras, $texto)
{
    print "\xA" . "----" . "\xA";
    print "Prueba censurador simple" . "\xA";
    print "----" . "\xA";

    $test = new CensuradorSimple($palabras);
    print $test($texto);

    print "\xA" . "----" . "\xA";
    print "FIN Prueba censurador simple" . "\xA";
    print "----" . "\xA";
}

function prueba_censurar_sin_bucles($palabras, $texto)
{
    print "\xA" . "----" . "\xA";
    print "Prueba censurador sin bucles" . "\xA";
    print "----" . "\xA";

    $test = new CensuradorSinBucles($palabras);
    print $test($texto);

    print "\xA" . "----" . "\xA";
    print "FIN Prueba censurador sin bucles" . "\xA";
    print "----" . "\xA";
}


function prueba_censurar_tipos($palabras, $texto)
{
    print "\xA" . "----" . "\xA";
    print "Prueba censurador tipos" . "\xA";
    print "----" . "\xA";

    $test = new CensuradorTipos($palabras);
    print $test($texto);

    print "\xA" . "----" . "\xA";
    print "FIN Prueba censurador tipos" . "\xA";
    print "----" . "\xA";
}

?>
