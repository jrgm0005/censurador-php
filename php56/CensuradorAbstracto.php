<?php

namespace Censurador;

abstract class CensuradorAbstracto
{
    protected $palabras_censuradas;

    public function __construct(array $palabras_censuradas)
    {
        error_log("CREANDO ABSTRACT", 3, "log.log");
        $this->palabras_censuradas = array_map('strtolower', $palabras_censuradas);
    }

    public function check_texto_y_palabras_a_censurar($palabras, $texto)
    {
        if(empty($texto))
            throw new Exception("Texto a censurar vacío", 1);

        if(empty($palabras))
            throw new Exception("No existen palabras a censurar", 1);
    }

    /**
     * Dado un texto, "censura" las palabras "cargadas" en el censurador,
     * sustituyéndolas en el texto por asteriscos, poniendo un número de
     * asteriscos equivalente al número de letras de la palabra seleccionada
     */
    // abstract public function __invoke(string $texto) : string;
}

?>
