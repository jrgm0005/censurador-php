<?php

namespace Censurador;

include_once("CensuradorAbstracto.php");
include_once("Helper.php");

class CensuradorSimple extends CensuradorAbstracto
{
    public function __invoke(string $texto) : string
    {
        try
        {
            if(empty($texto))
                throw new \Exception("Texto a censurar vacío", 1);

            if(empty($this->palabras_censuradas))
                throw new \Exception("No existen palabras a censurar", 1);

            return $this->censurar_texto($this->palabras_censuradas, $texto);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    private function censurar_texto($palabras, $texto) : string
    {
        foreach ($palabras as $key => $palabra)
        {
            $texto = $this->censurar_palabra($palabra, $texto);
        }
        return $texto;
    }


    private function censurar_palabra($palabra, $texto) : string
    {
        $reemplazo = $this->convertir_a_asteriscos($palabra);
        $texto_censurado = str_ireplace($palabra, $reemplazo, $texto);
        return $texto_censurado;
    }

    private function convertir_a_asteriscos($palabra) : string
    {
        return str_repeat("*", strlen($palabra));
    }
}
