<?php

namespace Censurador;

class Palabra
{
    public $palabra;

    public function __construct($palabra)
    {
        $this->palabra = $palabra;
    }

    public function censurar() : string
    {
        if(empty($this->palabra))
            return "";
        return str_repeat("*", strlen($this->palabra));
    }
}

?>
