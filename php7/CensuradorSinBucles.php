<?php

namespace Censurador;

include_once("CensuradorAbstracto.php");
include_once("Helper.php");

class CensuradorSinBucles extends CensuradorAbstracto
{
    private $array_reemplazo;

    public function __invoke(string $texto) : string
    {
        try
        {
            Helper::check_texto_y_palabras_a_censurar($this->palabras_censuradas, $texto);
            return $this->censurar_texto_sin_bucles($this->palabras_censuradas, $texto);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    /**
     * Función para censurar palabras en un texto sin bucles. (For, Foreach...)
     * Lo primero que he pensado es comprobar si hay una palabra a reemplazar, o hay más,
     * en caso de existir más, creo un array de reemplazo a partir del array de palabras a censurar
     * para poder realizar la censura sin usar ningún bucle.
     * @param  [type] $palabras [palabras a censurar]
     * @param  [type] $texto    [texto a censurar]
     * @return [type]           [texto con las palabras censuradas]
     */
    private function censurar_texto_sin_bucles($palabras, $texto) : string
    {
        if(is_array($palabras))
        {
            array_walk($palabras, array($this, 'crear_array_reemplazo'));
            $texto_censurado = str_ireplace($palabras, $this->array_reemplazo, $texto);
            return $texto_censurado;
        }
        else
        {
            $reemplazo = Helper::convertir_a_asteriscos($palabra);
            $texto_censurado = str_ireplace($palabras, $reemplazo, $texto);
            return $texto_censurado;
        }
    }

    private function crear_array_reemplazo($palabra)
    {
        if(empty($this->array_reemplazo))
            $this->array_reemplazo = array();
        array_push($this->array_reemplazo, Helper::convertir_a_asteriscos($palabra));
    }
}
