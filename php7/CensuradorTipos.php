<?php

namespace Censurador;

include_once("CensuradorAbstracto.php");
include_once("Helper.php");
include_once("Texto.php");

class CensuradorTipos extends CensuradorAbstracto
{
    public function __invoke(string $texto) : string
    {
        Helper::check_texto_y_palabras_a_censurar($this->palabras_censuradas, $texto);
        $texto_a_censurar = new Texto($texto, $this->palabras_censuradas);
        return $texto_a_censurar->censurar();
    }
}
