<?php

namespace Censurador;

class Helper
{
    /**
     * [Función que se le pasa una palabra a censurar y un texto.
     * Devolverá el texto con la palabra censurada sustituida por tantos * como sea la longitud de la palabra ]
     * @param  [type] $palabra [palabra a censurar]
     * @param  [type] $texto   [texto a censurar]
     * @return [type]          [texto con la palabra censurada]
     */
    public static function censurar_palabra($palabra, $texto) : string
    {
        $palabra_censurada = Helper::convertir_a_asteriscos($palabra);
        $texto_reemplazado = str_ireplace($palabra, $palabra_censurada, $texto);
        return $texto_reemplazado;
    }

    public static function check_texto_y_palabras_a_censurar($palabras, $texto)
    {
        if(empty($texto))
            throw new \Exception("Texto a censurar vacío", 1);

        if(empty($palabras))
            throw new \Exception("No existen palabras a censurar", 1);
    }

    public static function convertir_a_asteriscos($palabra) : string
    {
        return str_repeat("*", strlen($palabra));
    }

}
