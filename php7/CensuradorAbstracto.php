<?php

namespace Censurador;

abstract class CensuradorAbstracto
{
    protected $palabras_censuradas;

    public function __construct(array $palabras_censuradas)
    {
        $this->palabras_censuradas = array_map('strtolower', $palabras_censuradas);
    }

    /**
     * Dado un texto, "censura" las palabras "cargadas" en el censurador,
     * sustituyéndolas en el texto por asteriscos, poniendo un número de
     * asteriscos equivalente al número de letras de la palabra seleccionada
     */
    abstract public function __invoke(string $texto) : string;
}
