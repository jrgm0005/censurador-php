<?php

namespace Censurador;

include_once("Palabra.php");

class Texto
{
    public $texto;
    public $palabras_censuradas;

    public function __construct($texto, $palabras_censuradas)
    {
        $this->texto = $texto;
        $this->palabras_censuradas = array();
        foreach ($palabras_censuradas as $key => $palabra)
        {
            array_push($this->palabras_censuradas, new Palabra($palabra));
        }
    }

    public function censurar() : string
    {
        foreach ($this->palabras_censuradas as $key => $palabra_censurada)
        {
            $this->texto = str_ireplace($palabra_censurada->palabra, $palabra_censurada->censurar(), $this->texto);
        }
        return $this->texto;
    }
}

?>
